#ShanbayDict
扇贝词典是一个跨平台英语词典软件，C++ Qt5开发，无其它任何依赖库。较其它词典软件的优点是，生词可以加入扇贝网生词本，进入扇贝网的智能背单词计划。

查词主界面
![输入图片说明](http://git.oschina.net/uploads/images/2016/1222/095047_4cdb0ff2_362186.png "在这里输入图片标题")


快捷查词

![输入图片说明](http://git.oschina.net/uploads/images/2016/1222/095101_2b9808c9_362186.png "在这里输入图片标题")

点击查词图标

![输入图片说明](http://git.oschina.net/uploads/images/2016/1222/095124_72b8e494_362186.png "在这里输入图片标题")

软件设置窗口
![输入图片说明](http://git.oschina.net/uploads/images/2016/1222/095138_f54ffae6_362186.png "在这里输入图片标题")


关于

![输入图片说明](http://git.oschina.net/uploads/images/2016/1222/095154_9c730f43_362186.png "在这里输入图片标题")

 

扇贝词典 2.0.1 下载地址：https://pan.baidu.com/s/1mii80Pi 
本软件完全开源，放心使用，码云：https://git.oschina.net/lieefu/ShanbayDict